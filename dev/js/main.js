$(document).ready(function() {

    //hamburger
    $('#hamburger').click(function() {
        $(this).toggleClass('open');
        $('#nav').slideToggle('slow');
    });


    $('#hamburger-low-wrap').click(function() {
        $(this).find('#hamburger-low').toggleClass('open');
        $('#nav-low').slideToggle('slow');
    });

    // same height

    ;
    (function($, window, document, undefined) {
        'use strict';

        var $list = $('#services'),
            $items = $list.find('.service__description'),
            setHeights = function() {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function() {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);

    })(jQuery, window, document);

    ;
    (function($, window, document, undefined) {
        'use strict';

        var $list = $('#services'),
            $items = $list.find('.service__img'),
            setHeights = function() {
                $items.css('height', 'auto');

                var perRow = Math.floor($list.width() / $items.width());
                if (perRow == null || perRow < 2) return true;

                for (var i = 0, j = $items.length; i < j; i += perRow) {
                    var maxHeight = 0,
                        $row = $items.slice(i, i + perRow);

                    $row.each(function() {
                        var itemHeight = parseInt($(this).outerHeight());
                        if (itemHeight > maxHeight) maxHeight = itemHeight;
                    });
                    $row.css('height', maxHeight);
                }
            };

        setHeights();
        $(window).on('resize', setHeights);

    })(jQuery, window, document);





    // slider

    $('.slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span></span></div>',
        prevArrow: '<div class="slick-arrow-left"><span></span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        // autoplay:true,

        responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2


                }
            },
            {
                breakpoint: 690,
                settings: {
                    slidesToShow: 1

                }
            }


            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]

    });


    $('.sidebar-slider').slick({
        nextArrow: '<div class="slick-arrow-right"><span>&rang;</span></div>',
        prevArrow: '<div class="slick-arrow-left"><span>&lang;</span></div>',
        infinite: true,
        auto: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,



    });


    // slick slide same height
    var stHeight = $('.slider .slick-track').height();
    $('.slider .slick-slide').css('height', stHeight + 'px');

    // low nav on mob behavior
    var liWithUl = $("#nav-low > li").has('ul');
    $(window).on('load resize', function() {
        if ($(window).width() < 992) {
            liWithUl.addClass('toggleUL');
            // liWithUl.append("<span class='openUl'></span>");
            $('.openUl').remove();
            // !$('.openUl').hasClass('open').remove();
            liWithUl.append("<span class='openUl'></span>");


        } else {
            liWithUl.removeClass('toggleUL');
            $('.openUl').remove();
            // $("#nav-low > li .openUl").remove();


        }

    });


    $(window).on('load resize', function() {


        $('.openUl').click(function() {

            $(this).toggleClass('open');
            $(this).parent().find('ul').slideToggle('slow');
        });

    });


    //formstyler
    (function($) {
        $(function() {
            $('input, select').styler({

            });
        });
    })(jQuery);


    //phone mask
    $(function() {
        $('[name="phone"]').mask("+7(000)000-00-00", {
            clearIfNotMatch: true,
            placeholder: "+7(___)___-__-__"
        });
        $('[name="phone"]').focus(function(e) {
            if ($('[name="phone"]').val().length == 0) {
                $(this).val('+7(');
            }
        })
    });





});
