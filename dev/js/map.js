$(document).ready(function() {
    // ===============================================
    //MULTY TAG MAP
    // ===============================================
    // *
    // * Two maps on the same page
    // * 2015 - en.marnoto.com
    // *

    // necessary variables
    // *
    // * Two maps on the same page
    // * 2015 - en.marnoto.com
    // *

    // necessary variables
    var mapLeft, mapRight;
    var infoWindowLeft, infoWindowRight;
    var styleArray
    var mapOptions1;
    var mapOptions2;


    // markersData variable stores the information necessary to each marker
    var markersDataLeft = [{
        lat: 55.719526,
        lng: 37.605286,
        name: "Клиника лечения алкоголизма «Пробуждение»",
        address: "34400, г. Москва, ул. Донская, 33",
        phone: "8 (495) 223-22-55",
        mail: "info@lechenie-alko-moscow.ru" // don't insert comma in the last item of each marker
    }];

    var markersDataRight = [{
            lat: 40.6386333,
            lng: -8.745,
            name: "Клиника лечения алкоголизма «Пробуждение»",
            address: "34400, г. Москва, ул. Донская, 33",
            phone: "8 (495) 223-22-55",
            mail: "info@lechenie-alko-moscow.ru" // don't insert comma in the last item of each marker
        },
        {
            lat: 40.59955,
            lng: -8.7498167,
            name: "Клиника лечения алкоголизма «Пробуждение»",
            address: "34400, г. Москва, ул. Донская, 33",
            phone: "8 (495) 223-22-55",
            mail: "info@lechenie-alko-moscow.ru" // don't insert comma in the last item of each marker
        },
        {
            lat: 40.6247167,
            lng: -8.7129167,
            name: "Клиника лечения алкоголизма «Пробуждение»",
            address: "34400, г. Москва, ул. Донская, 33",
            phone: "8 (495) 223-22-55",
            mail: "info@lechenie-alko-moscow.ru" // don't insert comma in the last item of each marker
        } // don't insert comma in the last item
    ];

    function initialize(setMap) {

        styleArray = [{
            featureType: 'all',
            stylers: [{
                saturation: -80
            }]
        }, {
            featureType: 'road.arterial',
            elementType: 'geometry',
            stylers: [{
                    hue: '#00ffee'
                },
                {
                    saturation: 50
                }
            ]
        }, {
            featureType: 'poi.business',
            elementType: 'labels',
            stylers: [{
                visibility: 'off'
            }]
        }];




        if (setMap == "mapLeft") {
             mapOptions1 = {
                center: new google.maps.LatLng(55.720366, 37.605286),
                zoom: 17,
                styles: styleArray,
                scrollwheel: false
            };

            mapLeft = new google.maps.Map(document.getElementById('map1'), mapOptions1);

            // a new Info Window is created
            infoWindowLeft = new google.maps.InfoWindow();

            // Event that closes the Info Window with a click on the map
            google.maps.event.addListener(mapLeft, 'click', function() {
                infoWindowLeft.close();
            });

        } else {

             mapOptions2 = {
                center: new google.maps.LatLng(40.601203, -8.668173),
                zoom: 11,
                styles: styleArray,
                scrollwheel: false
            };

            mapRight = new google.maps.Map(document.getElementById('map2'), mapOptions2);

            // a new Info Window is created
            infoWindowRight = new google.maps.InfoWindow();

            // Event that closes the Info Window with a click on the map
            google.maps.event.addListener(mapRight, 'click', function() {
                infoWindowRight.close();
            });
        }

        // Finally displayMarkers() function is called to begin the markers creation
        displayMarkers(setMap);

        // Create second map only when initialize function is called for the first time.
        // Second time setMap is equal to mapRight, so this condition returns false and it will not run
        if (setMap == "mapLeft") {
            initialize("mapRight");
        }

    }
    google.maps.event.addDomListener(window, 'load', function() {
        initialize("mapLeft")
    });


    // This function will iterate over markersData array
    // creating markers with createMarker function
    function displayMarkers(setMap) {

        var markersData;
        var map;

        if (setMap == "mapLeft") {
            markersData = markersDataLeft;
            map = mapLeft;
        } else {
            markersData = markersDataRight;
            map = mapRight;
        }

        // this variable sets the map bounds according to markers position
        var bounds = new google.maps.LatLngBounds();

        // for loop traverses markersData array calling createMarker function for each marker
        for (var i = 0; i < markersData.length; i++) {

            var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
            var name = markersData[i].name;
            var address = markersData[i].address;
            var phone = markersData[i].phone;
            var mail = markersData[i].mail;

            createMarker(setMap, latlng, name, address, phone, mail);

            // marker position is added to bounds variable
            bounds.extend(latlng);
        }

        // Finally the bounds variable is used to set the map bounds
        // with fitBounds() function
        // map.fitBounds(bounds);
    }

    // This function creates each marker and it sets their Info Window content
    function createMarker(setMap, latlng, name, address, phone, mail) {

        var map;
        var infoWindow;

        if (setMap == "mapLeft") {
            map = mapLeft;
            infoWindow = infoWindowLeft;
        } else {
            map = mapRight;
            infoWindow = infoWindowRight;
        }

        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            title: name,
            icon: 'img/ico-mark.png'
        });

        // This event expects a click on a marker
        // When this event is fired the Info Window content is created
        // and the Info Window is opened.
        google.maps.event.addListener(marker, 'click', function() {

            // Creating the content to be inserted in the infowindow
            var iwContent = '<div id="iw_container">' +
                '<div class="iw_title">' + name + '</div>' +
                '<div class="iw_content">' +
                '<p class="iw_adress">' + address + '</p>' +

                '<p class="iw_phone">' + phone + '</p>' +

                '<p class="iw_mail">' + mail + '</p>' +

                '</div></div>';

            // including content to the Info Window.
            infoWindow.setContent(iwContent);

            // opening the Info Window in the current map and at the current marker location.
            infoWindow.open(map, marker);
        });
    }

});
